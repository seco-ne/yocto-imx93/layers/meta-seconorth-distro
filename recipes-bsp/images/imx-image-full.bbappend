IMAGE_INSTALL_QT6 = " \
    packagegroup-seco-qt6 \
    packagegroup-seco-qt6-tools \
    packagegroup-seco-qt6-translations \
"

IMAGE_INSTALL:append = " \
    alsa-utils \
    stressapptest \
    ${IMAGE_INSTALL_QT6} \
"

IMAGE_INSTALL:remove = " \
    packagegroup-imx-ml \
    packagegroup-qt6-imx \
"
