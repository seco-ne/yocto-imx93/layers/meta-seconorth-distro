DESCRIPTION = "SECO Qt6 Packagegroup"
SUMMARY = ""

# Prevents the dynamic renaming of packages
# (which throws an error in newer Bitbake versions)
PACKAGE_ARCH = "${TUNE_PKGARCH}"

inherit packagegroup

PACKAGES = " \
    ${PN} \
    ${PN}-tools \
    ${PN}-translations \
"

# Install fonts
QT5_FONTS = "ttf-dejavu-common ttf-dejavu-sans ttf-dejavu-sans-mono ttf-dejavu-serif "

QT5_QTQUICK3D = "qtquick3d"


RDEPENDS:${PN} = " \
    qt3d \
    qtbase \
    qtcharts \
    qtconnectivity \
    qtdatavis3d \
    qtdeclarative \
    qtimageformats \
    qtlocation \
    qtmqtt \
    qtmultimedia \
    qtnetworkauth \
    qtremoteobjects \
    qtserialbus \
    qtserialport \
    qtsvg \
    qttools \
    qtvirtualkeyboard \
    ${@bb.utils.contains('DISTRO_FEATURES', 'wayland', 'qtwayland qtwayland-plugins', '', d)} \
    ${@bb.utils.contains('DISTRO_FEATURES', 'x11', 'libxkbcommon', '', d)} \
    qtwebchannel \
    qtwebsockets \
    qtsensors \
    qtscxml \
    ${QT5_FONTS} \
    ${QT5_QTQUICK3D} \
"

RDEPENDS:${PN}-tools = " \
    qtdeclarative-tools \
    qttools-tools \
"

RDEPENDS:${PN}-translations = " \
    qttranslations-qtbase \
    qttranslations-qtconnectivity \
    qttranslations-qtlocation \
    qttranslations-qtmultimedia \
    qttranslations-qtserialport \
    qttranslations-qtwebsockets \
    qttranslations-qtwebengine \
    qttranslations-designer \
    qttranslations-linguist \
    qttranslations-assistant \ 
    qttranslations-qtdeclarative \
"
