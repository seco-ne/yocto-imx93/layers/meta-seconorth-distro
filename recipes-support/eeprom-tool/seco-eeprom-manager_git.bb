LICENSE = "GPL-2.0-or-later"
LIC_FILES_CHKSUM = "file://LICENSE;md5=639136791376fdaf0cd9f92ef1edce4a"
DESCRIPTION="SECO Eeprom Manager"

SRC_URI = "git://git.seco.com/clea-os/tools/seco-eeprom-manager.git;protocol=https;branch=develop"

PV = "1.0+git${SRCPV}"
SRCREV = "${AUTOREV}"

inherit autotools

INSANE_SKIP:${PN} += "ldflags"

EXTRA_OECONF = ""

FILES:${PN} += " ${bindir}/${PN} "

S = "${WORKDIR}/git"
B = "${WORKDIR}/git"

do_install() {
    mkdir -p ${D}/${bindir}
    install -m 775 ${B}/seco-eeprom-manager ${D}/${bindir}/${PN}
}
